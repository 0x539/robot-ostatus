#!/usr/bin/python3

from mastodon import Mastodon
from getpass import getpass
import locale
import re
from time import sleep

locale.setlocale(locale.LC_ALL, '')

def supprimer_balises(html):
    r = re.compile('<.*?>')
    texte_propre = re.sub(r, '', html)
    return texte_propre

def vers_csv(pouet, fichier):
    date = pouet['created_at']
    dico_utilisateur = pouet['account']
    nom_utilisateur = dico_utilisateur['acct']
    contenu = supprimer_balises(pouet['content'])
    liste_mots = contenu.split(' ')

    chaîne = '"' + str(date) + '";'
    chaîne += '"' + nom_utilisateur + '"'

    for mot in liste_mots:
        chaîne += ';"' + mot + '"'
    return chaîne + '\n'

adresse_IPA = 'https://framagit.org'
id_client, secret_client = Mastodon.create_app('narusite',
                                               api_base_url=adresse_IPA,
                                               to_file='sauvegarde')
m = Mastodon(id_client, secret_client, api_base_url=adresse_IPA, ratelimit_method='throw')

utilisateur = input('Nom d’utilisateur : ')
mdp = getpass('Mot de passe : ')

m.log_in(utilisateur, mdp, 'sauvegarde')

identifiant_max=None
try:
    id_max = open('identifiant_max', 'r')
    identifiant_max = int(id_max.readline().rstrip('\n'))
except:
    pass

fichier = open('pouets.csv', 'a')

nb_pouets = 0
while nb_pouets < 10000:
    print(' ' * 80, end='\r')
    print('Identifiant max : ' + str(identifiant_max), end='')
    pouets = []
    try:
        pouets_temporaires = m.timeline_local(max_id=identifiant_max)
    except Exception as e:
        print('%s : %s (limite atteinte ?)' % (type(e).__name__, e))
        sleep(300)
        print('lol')
    for pouet in pouets_temporaires:
        if pouet['language'] == 'fr':
            pouets.append(pouet)

    nb_pouets += len(pouets)
    print(', %d pouets lus.' % nb_pouets, end='')
    for pouet in pouets:
        fichier.write(vers_csv(pouet, fichier))

    if len(pouets) > 0:
        identifiant_max = pouets[len(pouets)-1]['id']
        f = open('identifiant_max', 'w')
        f.write(str(identifiant_max) + '\n')
        f.close()

fichier.close()
